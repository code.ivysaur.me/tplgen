# tplgen

![](https://img.shields.io/badge/written%20in-PHP-blue)

A language-agnostic template precompiler for mixing output strings with source code.

## Changelog

2013-02-25 r1
- Initial private release
- [⬇️ tplgen-r1.tar.xz](dist-archive/tplgen-r1.tar.xz) *(1.01 KiB)*

